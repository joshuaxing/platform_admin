/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-06-01 17:00:59
 * @LastEditTime: 2019-08-30 13:44:41
 * @LastEditors: Please set LastEditors
 */
import Vue from 'vue';
import Router from 'vue-router';
import store from '../vuex/index'
import http from '@/axios/http';
Vue.use(Router);
let router = new Router({
    mode: 'history',
    routes: [{
            path: '/',
            redirect: '/home'
        },
        {
            path: '/',
            component: resolve => require(['../components/page/Home/Index.vue'], resolve),
            meta: {
                title: '',
                requiresAuth: true
            },
            children: [{
                path: '/home',
                component: resolve => require(['../components/page/First/seo.vue'], resolve),
                    meta: {
                        title: '网站SEO',
                        requiresAuth: true
                    }
                },
                {
                    path: '/banner',
                    component: resolve => require(['../components/page/First/banner.vue'], resolve),
                    meta: {
                        title: '轮播图管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/link',
                    component: resolve => require(['../components/page/First/link.vue'], resolve),
                    meta: {
                        title: '友情链接',
                        requiresAuth: true
                    }
                },
                {
                    path: '/check',
                    component: resolve => require(['../components/page/Check/index.vue'], resolve),
                    meta: {
                        title: '审核列表',
                        requiresAuth: true
                    }
                },
                {
                    path: '/reproduct',
                    component: resolve => require(['../components/page/Recommend/product.vue'], resolve),
                    meta: {
                        title: '产品推荐',
                        requiresAuth: true
                    }
                },
                {
                    path: '/rehotel',
                    component: resolve => require(['../components/page/Recommend/hotel.vue'], resolve),
                    meta: {
                        title: '酒店推荐',
                        requiresAuth: true
                    }
                },
                {
                    path: '/recar',
                    component: resolve => require(['../components/page/Recommend/car.vue'], resolve),
                    meta: {
                        title: '婚车推荐',
                        requiresAuth: true
                    }
                },
                {
                    path: '/remerchant',
                    component: resolve => require(['../components/page/Recommend/merchant.vue'], resolve),
                    meta: {
                        title: '婚庆店推荐',
                        requiresAuth: true
                    }
                },
                {
                    path: '/recase',
                    component: resolve => require(['../components/page/Recommend/case.vue'], resolve),
                    meta: {
                        title: '案例推荐',
                        requiresAuth: true
                    }
                },
                {
                    path: '/mahotel',
                    component: resolve => require(['../components/page/Manage/hotel.vue'], resolve),
                    meta: {
                        title: '酒店商户',
                        requiresAuth: true
                    }
                },
                {
                    path: '/mawedding',
                    component: resolve => require(['../components/page/Manage/wedding.vue'], resolve),
                    meta: {
                        title: '婚庆商户',
                        requiresAuth: true
                    }
                },
                {
                    path: '/macar',
                    component: resolve => require(['../components/page/Manage/car.vue'], resolve),
                    meta: {
                        title: '婚车管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/masheying',
                    component: resolve => require(['../components/page/Manage/sheying.vue'], resolve),
                    meta: {
                        title: '摄影管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/masiyi',
                    component: resolve => require(['../components/page/Manage/siyi.vue'], resolve),
                    meta: {
                        title: '司仪管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/magenzhuang',
                    component: resolve => require(['../components/page/Manage/genzhuang.vue'], resolve),
                    meta: {
                        title: '跟妆管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/maproduct',
                    component: resolve => require(['../components/page/Manage/product.vue'], resolve),
                    meta: {
                        title: '产品管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/macase',
                    component: resolve => require(['../components/page/Manage/case.vue'], resolve),
                    meta: {
                        title: '案例管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/orwedding',
                    component: resolve => require(['../components/page/Order/door.vue'], resolve),
                    meta: {
                        title: '婚庆预约',
                        requiresAuth: true
                    }
                },
                {
                    path: '/orhotel',
                    component: resolve => require(['../components/page/Order/hotel.vue'], resolve),
                    meta: {
                        title: '酒店预约',
                        requiresAuth: true
                    }
                },
                {
                    path: '/orcar',
                    component: resolve => require(['../components/page/Order/car.vue'], resolve),
                    meta: {
                        title: '婚车预约',
                        requiresAuth: true
                    }
                },
                {
                    path: '/orindex',
                    component: resolve => require(['../components/page/Order/order.vue'], resolve),
                    meta: {
                        title: '订单列表',
                        requiresAuth: true
                    }
                },
                {
                    path: '/ordercat',
                    component: resolve => require(['../components/page/Order/catorder.vue'], resolve),
                    meta: {
                        title: '查看订单',
                        requiresAuth: true
                    }
                },
                {
                    path: '/desindex',
                    component: resolve => require(['../components/page/Design/design.vue'], resolve),
                    meta: {
                        title: '设计师列表',
                        requiresAuth: true
                    }
                },
                {
                    path: '/desproduct',
                    component: resolve => require(['../components/page/Design/product.vue'], resolve),
                    meta: {
                        title: '设计产品',
                        requiresAuth: true
                    }
                }
            ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Admin.vue'], resolve)
        },
        {
            path: '*',
            redirect: '/404'
        },
        {
            path: '/404',
            component: resolve => require(['../components/page/404.vue'], resolve)
        }
    ]
})

router.beforeEach((to, from, next) => {
    if (window.sessionStorage.getItem('xuuadmin')) {
      //登录信息
      store.commit({
        type: 'gotAdminInfo',
        data: JSON.parse(window.sessionStorage.getItem('xuuadmin'))
      })
    }
    if (to.matched.some(record => record.meta.requiresAuth)) {
        const id = store.state.uuadmin.adminid;
        if (id) {
          next()
        } else {
          next('/login')
        }
    } else {
      next() // 确保一定要调用 next()
    }
})
router.beforeResolve((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        http.post('/admin/getmysession.do')
        .then((result) => {
            if (result.status === 0) {
                let value = {
                    areaid: 0,
                    cityid: 793,
                    adminid: 0,
                    name: '',
                    loginname: ''
                }
                store.commit({
                    type: 'gotAdminInfo',
                    data: value
                })
                window.sessionStorage.removeItem('xuuadmin')
                next({
                    path: '/login'
                })
            } else {
                next()
            }
        })
    } else {
        next()
    }
        
})
export default router;
